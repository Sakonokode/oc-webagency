var path = './public/images/slider/';
var currentSlide = 1;
var minSlide = 1;
var maxSlide = 2;

function showSlide(n) {
    currentSlide += n;

    if(currentSlide < minSlide)
        currentSlide = maxSlide;

    if(currentSlide > maxSlide)
        currentSlide = minSlide;

    var url = 'url(' + path + 'bg' + currentSlide + '.jpg)',
        container = document.getElementById('slide');

    container.style.backgroundImage = url;
}